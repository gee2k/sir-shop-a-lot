
### Sir Shop a lot ###

![nexus-5-mockup_B.png](https://bitbucket.org/repo/bMrXnj/images/4054254173-nexus-5-mockup_B.png)

# Authors #

Pascal Stüdlein 549433
Georg Weidel 549439


# Language #

* Java
* html

# Description #

Projekt wurde im Rahmen des Moduls Mobile Anwendungen als Teamarbeit angefertigt.

Die App ist als Einkaufsliste gedacht, welche man bequem durch scannen des Barcodes des Produktes füllen kann.
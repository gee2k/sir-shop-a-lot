package de.htw_berlin.gma.database;

import android.provider.BaseColumns;

/**
 * Created by Pascal & Georg on 25/12/15.
 */
public final class ShoppingListContract {

    public ShoppingListContract() {}

    public static abstract class ShoppingListItem implements BaseColumns {
        public static final String TABLE_NAME = "shopping_list";
        public static final String COLUMN_NAME_EAN = "ean";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_BOUGHT = "bought";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE  = " INTEGER";
    private static final String COMMA_SEP = ",";
    protected static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ShoppingListItem.TABLE_NAME + " (" +
                    ShoppingListItem._ID + " INTEGER PRIMARY KEY," +
                    ShoppingListItem.COLUMN_NAME_EAN + TEXT_TYPE + COMMA_SEP +
                    ShoppingListItem.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ShoppingListItem.COLUMN_NAME_BOUGHT + INT_TYPE +
                    ")";
    protected static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ShoppingListItem.TABLE_NAME;

}

package de.htw_berlin.gma.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import de.htw_berlin.gma.model.EanItem;

/**
 * Created by Pascal & Georg on 25/12/15.
 */
public class ShoppingListDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "ShoopingList.db";


    public ShoppingListDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ShoppingListDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ShoppingListContract.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ShoppingListContract.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public Boolean saveEanItem(EanItem item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ShoppingListContract.ShoppingListItem.COLUMN_NAME_NAME, item.getName());
        values.put(ShoppingListContract.ShoppingListItem.COLUMN_NAME_EAN, item.getEanIdentifier());
        values.put(ShoppingListContract.ShoppingListItem.COLUMN_NAME_BOUGHT, item.isBought());

        long primaryKey;
        primaryKey = db.insert(ShoppingListContract.ShoppingListItem.TABLE_NAME, null, values);

        item.setItemId(primaryKey);

        return primaryKey != -1;
    }

    public ArrayList<EanItem> getAllItems() {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                ShoppingListContract.ShoppingListItem._ID,
                ShoppingListContract.ShoppingListItem.COLUMN_NAME_EAN,
                ShoppingListContract.ShoppingListItem.COLUMN_NAME_NAME,
                ShoppingListContract.ShoppingListItem.COLUMN_NAME_BOUGHT
        };

        String sortOrder =
                ShoppingListContract.ShoppingListItem._ID + " DESC";


        Cursor c = db.query(
                ShoppingListContract.ShoppingListItem.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        ArrayList eanItems = new ArrayList<EanItem>();

        if(c.moveToFirst()) {
            do {
                long itemId = c.getLong(c.getColumnIndexOrThrow(ShoppingListContract.ShoppingListItem._ID));
                String eanIdentifier = c.getString(c.getColumnIndexOrThrow(ShoppingListContract.ShoppingListItem.COLUMN_NAME_EAN));
                String name = c.getString(c.getColumnIndexOrThrow(ShoppingListContract.ShoppingListItem.COLUMN_NAME_NAME));
                int bought = c.getInt(c.getColumnIndexOrThrow(ShoppingListContract.ShoppingListItem.COLUMN_NAME_BOUGHT));

                EanItem newItem = new EanItem(itemId, eanIdentifier, name, bought == 1);
                eanItems.add(newItem);
            } while (c.moveToNext());
        }

        return eanItems;
    }

    public void deleteItem(EanItem item) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selection = ShoppingListContract.ShoppingListItem._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(item.getItemId()) };
        db.delete(ShoppingListContract.ShoppingListItem.TABLE_NAME, selection, selectionArgs);
    }

    public void deleteAllItems() {
        SQLiteDatabase db = this.getReadableDatabase();

        db.delete(ShoppingListContract.ShoppingListItem.TABLE_NAME, null, null);
    }

    public void toggleBoughtState(EanItem eanItem) {
        eanItem.toggleBought();

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(ShoppingListContract.ShoppingListItem.COLUMN_NAME_BOUGHT, eanItem.isBought());

        String selection = ShoppingListContract.ShoppingListItem._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(eanItem.getItemId()) };

        int count = db.update(
                ShoppingListContract.ShoppingListItem.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        System.out.println(count);
    }
}

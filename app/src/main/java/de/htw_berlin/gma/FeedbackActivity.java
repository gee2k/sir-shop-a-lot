package de.htw_berlin.gma;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

/**
 * Created by Pascal & Georg on 09/01/16.
 */
public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity);
        setTitle(R.string.feedbackActivityTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_send:
                sendMailButtonPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void sendMailButtonPressed() {
        EditText text = (EditText) findViewById(R.id.editText);

        if (text.getText().length() == 0) {
            showDialog();
        } else {
            sendMail();
        }
    }

    private void showDialog() {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(R.string.feedback_dialog_title);
        alert.setMessage(getResources().getString(R.string.feedback_dialog_text));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.show();
    }
    private void sendMail() {
        EditText text = (EditText) findViewById(R.id.editText);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setType("text/plain");  //todo setType in string.xml ?
        emailIntent.setData(Uri.parse("mailto:"));  //todo setData mailto in strings.xml
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "mail@pascal-stuedlein.de", "georg.weidel@googlemail.com" });
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_email_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, text.getText().toString());

        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(emailIntent, 1);
        } // todo check if we can catch unsupported actions. maybe ask mr obermann
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EditText text = (EditText) findViewById(R.id.editText);
        text.setText("");
    }
}

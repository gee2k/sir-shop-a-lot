package de.htw_berlin.gma.model;

/**
 * Created by Pascal & Georg on 25/12/15.
 */
public class EanItem {
    private long itemId;
    private String eanIdentifier;
    private String name;
    private boolean bought;

    public EanItem(String eanIdentifier, String name) {
        this(-1, eanIdentifier, name);
    }

    public EanItem(long itemId, String eanIdentifier, String name) {
        this(itemId, eanIdentifier, name, false);
    }

    public EanItem(long itemId, String eanIdentifier, String name, boolean bought) {
        this.itemId = itemId;
        this.eanIdentifier = eanIdentifier;
        this.name = name;
        this.bought = bought;
    }

    public String getEanIdentifier() {
        return eanIdentifier;
    }

    public String getName() {
        return name;
    }

    //todo shouldnt be public, we could think about another architecture
    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getItemId() {
        return itemId;
    }

    public boolean isBought() {
        return bought;
    }

    public void toggleBought() {
        bought = !bought;
    }

    public String printDebug() {
        return "EanItem{" +
                "itemId=" + itemId +
                ", eanIdentifier='" + eanIdentifier + '\'' +
                ", name='" + name + '\'' +
                ", bought=" + bought +
                '}';
    }

    @Override
    public String toString() {
        return name;
    }
}

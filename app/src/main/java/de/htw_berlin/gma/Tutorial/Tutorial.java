package de.htw_berlin.gma.Tutorial;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import de.htw_berlin.gma.R;
import de.htw_berlin.gma.database.ShoppingListDbHelper;
import de.htw_berlin.gma.model.EanItem;

/**
 * Created by Pascal & Georg on 1/13/16.
 */
public class Tutorial extends Activity {

    private SharedPreferences settings;
    private ShoppingListDbHelper dbHelper;
    private Context context;

    private static final String PREFS_NAME = "MyPrefsFile";
    private static final String FIRST_RUN_KEY = "firstRun";


    public Tutorial(Context context, ShoppingListDbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
        this.context = context;
    }

    public void runTutorial()
    {
        if (checkForFirstRun())
        {
            addDummyData();
            setFirstRun();
        }
    }

    private boolean checkForFirstRun()
    {
        return context.getSharedPreferences(PREFS_NAME, 0).getBoolean(FIRST_RUN_KEY, true);       //reads firstRun from preferences if exist. Creates it with true if not
    }

    private void setFirstRun()
    {
        //set values to false.
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putBoolean(FIRST_RUN_KEY, false);
        editor.apply();
    }

    private void addDummyData() {

        String[] tutStrings = context.getResources().getStringArray(R.array.tutorial);

            //Tutorial EAN Items
        if (tutStrings != null) {
            dbHelper.saveEanItem(new EanItem("4", tutStrings[3]));
            dbHelper.saveEanItem(new EanItem("3", tutStrings[2]));
            dbHelper.saveEanItem(new EanItem("2", tutStrings[1]));

            // last item with set check mark
            EanItem tabItem = new EanItem("1", tutStrings[0]);
            tabItem.toggleBought();
            dbHelper.saveEanItem(tabItem);
        }
        else
        {
            System.out.println("array nicht initialisiert...");
        }
    }
}
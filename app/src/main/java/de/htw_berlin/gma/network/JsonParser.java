package de.htw_berlin.gma.network;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStreamReader;

import de.htw_berlin.gma.model.EanItem;

public class JsonParser {

    private JsonReader jsonReader;
    private EanItem eanItem;

    public JsonParser(InputStreamReader streamReader) {
        jsonReader = new JsonReader(streamReader);
    }

    public EanItem parseObject() throws InvalidEanDataException, IOException {
        String name = null;
        String identifier = null;

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            if (key.equals("gtin")) {
                identifier = jsonReader.nextString();
            } else if (key.equals("name") && jsonReader.peek() != JsonToken.NULL) {
                name = jsonReader.nextString();
            } else {
                jsonReader.skipValue();
            }
        }

        //todo maybe use dedicated exception
        if (name == null || identifier == null) {
            throw new InvalidEanDataException();
        }

        return new EanItem(identifier, name);
    }
}

package de.htw_berlin.gma.network;

import java.util.UUID;

import de.htw_berlin.gma.model.EanItem;

/**
 * Created by Pascal & Georg on 21/12/15.
 */

public interface NetworkCallback {
    void networkRequestFinished(UUID requestUuid, EanItem response);
    void networkRequestFailed(UUID requestUuid, Exception error);
}

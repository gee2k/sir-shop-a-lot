package de.htw_berlin.gma.network;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.UUID;

import de.htw_berlin.gma.model.EanItem;

public class NetworkManager {
    private static NetworkManager sharedInstance = null;

    private static final String BASE_URL = "https://api.outpan.com/v2/products/%s?apikey=e2520b02dc2c5fccef8c0cf76cfe07ce";
    private static final int READ_TIMEOUT = 10000;
    private static final int CONNECT_TIMEOUT = 15000;

    private HashMap<UUID, NetworkCallback> callbacks;

    // todo singleton might not be necessary
    public static NetworkManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new NetworkManager();
        }

        return sharedInstance;
    }

    public NetworkManager() {
        this.callbacks = new HashMap<>();
    }

    public void retrieveItem(NetworkCallback target, String identifier) {
        JSONAsyncTask asyncTask = (JSONAsyncTask) new JSONAsyncTask();
        this.callbacks.put(asyncTask.getRequestUuid(), target);
        String requestString = String.format(BASE_URL, identifier);
        asyncTask.execute(requestString);
    }

    private class JSONAsyncTask extends AsyncTask<String, Void, Object> {
        private UUID requestUuid;

        public JSONAsyncTask() {
            this.requestUuid = UUID.randomUUID();
        }

        public UUID getRequestUuid() {
            return requestUuid;
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                if (params.length > 0) {
                    URL requestURL = new URL(params[0]);
                    return retrieveResponse(requestURL);
                } else {
                    throw new Exception("URL is needed");
                }
            } catch (Exception e) {
                return e;
            }
        }

        @Override
        protected void onPostExecute(Object aObject) {
            super.onPostExecute(aObject);

            if (aObject instanceof EanItem) {
                callbacks.get(this.requestUuid).networkRequestFinished(this.requestUuid, (EanItem)aObject);
            } else if (aObject instanceof Exception) {
                callbacks.get(this.requestUuid).networkRequestFailed(this.requestUuid, (Exception)aObject);
            }

            callbacks.remove(this.requestUuid);
        }
    }

    private EanItem retrieveResponse(URL url) throws IOException, InvalidEanDataException {
        InputStream is = null;

        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT /* milliseconds */);
            conn.setConnectTimeout(CONNECT_TIMEOUT /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();

            is = conn.getInputStream();

            //todo dont like the limit of 500
            return readStream(is, 500);

        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public EanItem readStream(InputStream stream, int len) throws IOException, InvalidEanDataException {
        InputStreamReader reader;
        reader = new InputStreamReader(stream, "UTF-8");
        JsonParser parser = new JsonParser(reader);
        return parser.parseObject();
    }
}
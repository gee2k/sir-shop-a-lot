package de.htw_berlin.gma;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.htw_berlin.gma.Tutorial.Tutorial;
import de.htw_berlin.gma.database.ShoppingListDbHelper;
import de.htw_berlin.gma.model.EanItem;
import de.htw_berlin.gma.network.InvalidEanDataException;
import de.htw_berlin.gma.network.NetworkCallback;
import de.htw_berlin.gma.network.NetworkManager;
import de.htw_berlin.gma.view.EanArrayAdapter;
import de.htw_berlin.gma.view.PartyButton;
import de.htw_berlin.gma.view.PartyButtonClickListener;

public class MainActivity extends AppCompatActivity implements NetworkCallback, PartyButtonClickListener, DialogInterface.OnClickListener {

    private PartyButton partyButton;
    private ShoppingListDbHelper dbHelper;
    private EanArrayAdapter<EanItem> eanArrayAdapter;
    private Toast currentToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        partyButton = (PartyButton) findViewById(R.id.partyButton);
        partyButton.setPartyButtonClickListener(this);

        // database code
        try {
            dbHelper = new ShoppingListDbHelper(getApplicationContext());
            dbHelper.getWritableDatabase();

            // create Tutorial
            Tutorial tutorial = new Tutorial(this, dbHelper);
            tutorial.runTutorial();

            //populate List
            populateListView(dbHelper);
        } catch (Exception e) {
            System.out.println(e);
        }
        //react to clicks on list
        registerClickCallback();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent;
        switch (id) {
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                this.startActivity(intent);
                break;
            case R.id.action_reset_list:
                if (dbHelper.getAllItems().size() > 0)
                    askDeleteAllDataDialog();
                break;
            default:
                super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void populateListView(ShoppingListDbHelper dbHelper) {


        // Build Adapter and fill ListView with content
        eanArrayAdapter = new EanArrayAdapter<>(
                this,                              // Context for activity
                R.layout.row,                      // Layout to use (create)
                dbHelper.getAllItems());           // Items to be displayed


        //Configure the Listview
        ListView listView = (ListView) findViewById(R.id.listViewMain);
        listView.setAdapter(eanArrayAdapter);
    }

    // clicks on the list items are handled here
    private void registerClickCallback() {

        final ListView listView = (ListView) findViewById(R.id.listViewMain);

        // normal clicks are handeled here
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                EanItem helperItem = (EanItem)listView.getItemAtPosition(position);
                ImageView helperImageView = (ImageView)view.findViewById(R.id.checkMark);

                dbHelper.toggleBoughtState(helperItem);

                if (helperItem.isBought()) {
                    helperImageView.setImageResource(R.mipmap.check_orange);
                } else {
                    helperImageView.setImageResource(R.mipmap.check_wht_2);
                }
            }
        });

        // aaaaaaaand the magic loooooooong click listener ;)
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {

                Object deleteObject = listView.getItemAtPosition(position);

                dbHelper.deleteItem((EanItem)deleteObject);
                eanArrayAdapter.remove(deleteObject);
                toaster(getString(R.string.itemDeleted));
                return true;
            }
        });
    }

    private void addNewItem(EanItem addItem) {
        if (addItem != null) {
            dbHelper.saveEanItem(addItem);
            eanArrayAdapter.insert(addItem, 0);
        }
    }

    private void addNewItem(String itemTitle) {
        addNewItem(new EanItem(null, itemTitle));
    }

    @Override
    public void qrButtonClicked() {
        if (isConnectedToInternet()) {
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);   //create new scanintegrator

            List<String> supportedBarCodes = new ArrayList<String>();
            supportedBarCodes.add("EAN_8");
            supportedBarCodes.add("EAN_13");

            scanIntegrator.initiateScan(supportedBarCodes);
        } else {
            askCreateItemDialog(getResources().getString(R.string.no_internet_no_scanner));
        }
    }

    private void deleteData()
    {
        // delete items from db
        dbHelper.deleteAllItems();

        //delete items from list view
        eanArrayAdapter.clear();
    }

    private void askDeleteAllDataDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.deleteAllDataTitle));
        alert.setMessage(getResources().getString(R.string.deleteAllDataText));
        alert.setPositiveButton(getResources().getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deleteData();
            }
        });
        alert.setNegativeButton(getResources().getString(R.string.button_no), this);
        alert.show();
    }

    private void askCreateItemDialog(String title) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(title);
        alert.setMessage(getResources().getString(R.string.create_manually));
        alert.setPositiveButton(getResources().getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                addButtonClicked();
            }
        });
        alert.setNegativeButton(getResources().getString(R.string.button_no), this);
        alert.show();
    }

    private boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void addButtonClicked() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.create_new_ean));

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        alert.setView(input);
        alert.setPositiveButton(getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String itemTitle = input.getText().toString();
                addNewItem(itemTitle);
            }
        });
        alert.setNegativeButton(getResources().getString(R.string.button_cancel), this);

        final AlertDialog dialog = alert.create();
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("after");
                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(input.getText().length() != 0);
            }
        });

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (scanningResult.getContents() != null)
        {
            String scanContent = scanningResult.getContents();
            partyButton.isLoading(true);
            NetworkManager.getInstance().retrieveItem(this, scanContent);
        }
    }


    @Override
    public void networkRequestFinished(UUID requestUuid, EanItem response) {

        partyButton.isLoading(false);

        addNewItem(response);
        toaster(String.format(getString(R.string.networkRequestFinishedMessage), response));
    }

    @Override
    public void networkRequestFailed(UUID requestUuid, Exception error) {

        partyButton.isLoading(false);

        if (error instanceof InvalidEanDataException) {
            askCreateItemDialog(getResources().getString(R.string.item_not_found));
        } else {
            toaster(getString(R.string.unknownError));
        }
    }

    // todo if time put toaster into dedicated class maybe with a queue
    public void toaster(final String message) {
        if (this.currentToast != null) {
            this.currentToast.cancel();
        }

        this.currentToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        this.currentToast.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.cancel();
        }
    }
}

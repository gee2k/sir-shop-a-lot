package de.htw_berlin.gma;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Pascal & Georg on 08/01/16.
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);
        setTitle(getResources().getString(R.string.about_title));

        String contentString = htmlString();
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.loadDataWithBaseURL("file:///android_asset/", contentString, "text/html", "utf-8", null);
    }

    private String htmlString() {
        String contentString = null;
        try {
            InputStream inputStream=getAssets().open("about.html");
            contentString = streamToString(inputStream);
            contentString = contentString.replace("__app_name__", getString(R.string.app_name));
            contentString = contentString.replace("__version__", getVersionName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentString;
    }

    private String getVersionName() throws Exception{
        PackageManager manager = getApplicationContext().getPackageManager();
        PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
        return info.versionName;
    }

    private String streamToString(InputStream stream) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder builder = new StringBuilder();
        String line = null;

        while ((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }
        reader.close();

        return builder.toString();
    }

}
package de.htw_berlin.gma.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.htw_berlin.gma.R;
import de.htw_berlin.gma.model.EanItem;

/**
 * Created by Pascal & Georg on 1/8/16.
 */
public class EanArrayAdapter<T> extends ArrayAdapter
{
    static EanItem eanItem;

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }

    public EanArrayAdapter(Context context, @LayoutRes int resource, @NonNull List<T> objects) {

        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        //infalter checks Layout and ...
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row, parent, false);

            // create the holder
            holder = new ViewHolder();

            // connect the holder
            holder.textView = (TextView) convertView.findViewById(R.id.list_item_textview_new);
            holder.imageView = (ImageView) convertView.findViewById(R.id.checkMark);

            convertView.setTag(holder);
        }
        else
        {
            // so just use the holder then do a find by resource every time
            holder = (ViewHolder)convertView.getTag();
        }


        // finally assign values like texts and the checkmark
        holder.textView.setText(getItem(position).toString());

        // initial set of checked image
        // maybe read the bought state from the object out of the list given to the view.

        eanItem = (EanItem)getItem(position);

        if (eanItem.isBought()) {
            holder.imageView.setImageResource(R.mipmap.check_orange);
        } else {
            holder.imageView.setImageResource(R.mipmap.check_wht_2);
        }

        convertView.setBackgroundColor(Color.WHITE);
        return convertView;
    }
}
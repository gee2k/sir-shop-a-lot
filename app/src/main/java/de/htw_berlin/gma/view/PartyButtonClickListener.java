package de.htw_berlin.gma.view;

/**
 * Created by Pascal & Georg on 05/01/16.
 */
public interface PartyButtonClickListener {
    public void qrButtonClicked();
    public void addButtonClicked();
}

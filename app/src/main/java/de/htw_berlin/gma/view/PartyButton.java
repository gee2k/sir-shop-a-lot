package de.htw_berlin.gma.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import de.htw_berlin.gma.R;

/**
 * Created by Pascal & Georg on 04/01/16.
 */
public class PartyButton extends CoordinatorLayout implements View.OnClickListener {

    private enum ViewState {
        Collapsed, Expanded
    }

    private com.github.clans.fab.FloatingActionButton triggerButton;
    private FloatingActionButton qrReaderButton;
    private FloatingActionButton addButton;
    private Context context;
    private ViewState viewState;
    private PartyButtonClickListener partyButtonClickListener;
    private ColorStateList primaryColor;
    private ColorStateList secondaryColor;

    public PartyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
        this.viewState = ViewState.Collapsed;
    }

    public PartyButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PartyButton);
        this.primaryColor = ta.getColorStateList(R.styleable.PartyButton_primaryColor);
        this.secondaryColor = ta.getColorStateList(R.styleable.PartyButton_secondaryColor);

        this.context = context;
        this.viewState = ViewState.Collapsed;

        createTriggerButton();

        CoordinatorLayout.LayoutParams fabLayout = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fabLayout.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        fabLayout.setMargins(0, 0, 40, 40);

        createQrReaderButton(fabLayout);
        createAddButton(context, fabLayout);

        addView(this.qrReaderButton);
        addView(this.addButton);
        addView(this.triggerButton);
    }

    public void isLoading(Boolean loading) {
        this.triggerButton.setIndeterminate(loading);
        this.triggerButton.setClickable(!loading);
    }

    private void createAddButton(Context context, LayoutParams fabLayout) {
        this.addButton = new FloatingActionButton(context);
        this.addButton.setOnClickListener(this);
        this.addButton.setLayoutParams(fabLayout);
        this.addButton.setImageResource(android.R.drawable.ic_menu_add);
        this.addButton.setBackgroundTintList(this.secondaryColor);
        this.addButton.setAlpha(0f);
    }

    private void createQrReaderButton(LayoutParams fabLayout) {
        this.qrReaderButton = new FloatingActionButton(this.context);
        this.qrReaderButton.setOnClickListener(this);
        this.qrReaderButton.setLayoutParams(fabLayout);
        this.qrReaderButton.setImageResource(android.R.drawable.ic_menu_camera);
        this.qrReaderButton.setBackgroundTintList(this.secondaryColor);
        this.qrReaderButton.setAlpha(0f);
    }

    private void createTriggerButton() {
        this.triggerButton = new com.github.clans.fab.FloatingActionButton(this.context);
        this.triggerButton.setOnClickListener(this);
        this.triggerButton.setImageResource(android.R.drawable.ic_menu_add);
        this.triggerButton.setShadowXOffset(3f);
        this.triggerButton.setColorPressed(this.primaryColor.getDefaultColor());
        this.triggerButton.setColorNormal(this.primaryColor.getDefaultColor());
        LayoutParams triggerButtonLayout = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        triggerButtonLayout.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        triggerButtonLayout.setMargins(0,
                0,
                40 - this.triggerButton.getShadowRadius() - this.triggerButton.getShadowXOffset(),
                40 - this.triggerButton.getShadowRadius() - this.triggerButton.getShadowYOffset());
        this.triggerButton.setLayoutParams(triggerButtonLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.triggerButton.setZ(999);
        }
    }

    public PartyButton(Context context) {
        super(context);

        this.context = context;
        this.viewState = ViewState.Collapsed;
    }

    private void animateButton() {
        AnimatorFactory animatorFactory = new AnimatorFactory();
        AnimatorSet buttonAnimatorSet;
        if (this.viewState == ViewState.Collapsed) {
            this.viewState = ViewState.Expanded;
            buttonAnimatorSet = animatorFactory.getExpandAnimator(this.triggerButton, this.addButton, this.qrReaderButton);
        } else {
            this.viewState = ViewState.Collapsed;
            buttonAnimatorSet = animatorFactory.getCollapseAnimator(this.triggerButton, this.addButton, this.qrReaderButton);
        }
        buttonAnimatorSet.start();
    }

    public void onClick(View v) {
        if (v == this.triggerButton) {
            animateButton();
        } else if (this.partyButtonClickListener != null) {
            animateButton();

            if (v == this.qrReaderButton) {
                this.partyButtonClickListener.qrButtonClicked();
            } else if (v == this.addButton) {
                this.partyButtonClickListener.addButtonClicked();
            }
        }
    }

    public void setPartyButtonClickListener(PartyButtonClickListener partyButtonClickListener) {
        this.partyButtonClickListener = partyButtonClickListener;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    private class AnimatorFactory {

        private final float TRIGGER_ROTATION_DEGREE = 45f;
        private final float ROTATION_DEGREE = -90f;
        private final float SMALL_INDENT = -5;
        private final float BIG_INDENT = -65f;
        private final int ANIMATION_DURATION = 250;

        public AnimatorSet getExpandAnimator(View triggerView, View horizontolView, View verticalView) {
            AnimatorSet buttonAnimatorSet = new AnimatorSet();
            buttonAnimatorSet.play(ObjectAnimator.ofFloat(triggerView, "rotation", 0, TRIGGER_ROTATION_DEGREE));
            buttonAnimatorSet.play(this.getVerticalExpandAnimator(verticalView));
            buttonAnimatorSet.play(this.getHorizontalExpandAnimator(horizontolView));
            buttonAnimatorSet.play(this.getExpandAlphaAnimator(verticalView, horizontolView));
            buttonAnimatorSet.setDuration(ANIMATION_DURATION);

            return buttonAnimatorSet;
        }

        public AnimatorSet getCollapseAnimator(View triggerView, View horizontolView, View verticalView) {
            AnimatorSet buttonAnimatorSet = new AnimatorSet();
            buttonAnimatorSet.play(ObjectAnimator.ofFloat(triggerView, "rotation", TRIGGER_ROTATION_DEGREE, 0));
            buttonAnimatorSet.play(this.getVerticalCollapseAnimator(verticalView));
            buttonAnimatorSet.play(this.getHorizontalCollapseAnimator(horizontolView));
            buttonAnimatorSet.play(this.getCollapseAlphaAnimator(horizontolView, verticalView));
            buttonAnimatorSet.setDuration(ANIMATION_DURATION);

            return buttonAnimatorSet;
        }

        public AnimatorSet getExpandAlphaAnimator(View ... v) {
            AnimatorSet buttonAnimatorSet = new AnimatorSet();
            for (int i = 0; i < v.length; i++) {
                buttonAnimatorSet.play(ObjectAnimator.ofFloat(v[i], "alpha", 0f, 1f));
            }
            buttonAnimatorSet.setDuration(ANIMATION_DURATION/3);
            return buttonAnimatorSet;
        }

        public AnimatorSet getCollapseAlphaAnimator(View ... v) {
            AnimatorSet buttonAnimatorSet = new AnimatorSet();
            for (int i = 0; i < v.length; i++) {
                buttonAnimatorSet.play(ObjectAnimator.ofFloat(v[i], "alpha", 1f, 0f));
            }

            long duration = ANIMATION_DURATION / 3;
            buttonAnimatorSet.setDuration(duration * 2);
            buttonAnimatorSet.setStartDelay(duration);

            return buttonAnimatorSet;
        }

        private AnimatorSet getVerticalExpandAnimator(View view) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationX", 0f, pxToDp(SMALL_INDENT)));
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationY", 0f, pxToDp(BIG_INDENT)));
            animatorSet.play(ObjectAnimator.ofFloat(view, "rotation", ROTATION_DEGREE, 0f));

            return animatorSet;
        }

        private AnimatorSet getHorizontalExpandAnimator(View view) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationY", 0f, pxToDp(SMALL_INDENT)));
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationX", 0f, pxToDp(BIG_INDENT)));
            animatorSet.play(ObjectAnimator.ofFloat(view, "rotation", 0f, ROTATION_DEGREE));

            return animatorSet;
        }

        private AnimatorSet getVerticalCollapseAnimator(View view) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationX", pxToDp(SMALL_INDENT), 0f));
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationY", pxToDp(BIG_INDENT), 0f));
            animatorSet.play(ObjectAnimator.ofFloat(view, "rotation", 0f, ROTATION_DEGREE));

            return animatorSet;
        }

        private AnimatorSet getHorizontalCollapseAnimator(View view) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationY", pxToDp(SMALL_INDENT), 0f));
            animatorSet.play(ObjectAnimator.ofFloat(view, "translationX", pxToDp(BIG_INDENT), 0f));
            animatorSet.play(ObjectAnimator.ofFloat(view, "rotation", ROTATION_DEGREE, 0f));

            return animatorSet;
        }

        public float pxToDp(float px) {
            Resources r = getResources();
            px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, r.getDisplayMetrics());
//
//            DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
//            float dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
            return px;
        }

    }
}

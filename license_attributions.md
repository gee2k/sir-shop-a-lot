# License attributions

## Licenses for List Icons

- Material Design by google (http://www.flaticon.com/free-icon/circle-with-check-symbol_60778#term=check&page=1&position=21)

## Licenses for App Icon
- "Shopping cart icon" Designed by Kreativkolors - Freepik.com
- "Phone Icon" by Vivien Bocquelet http://vivienbocquelet.fr


